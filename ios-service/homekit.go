package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/brutella/hc"
	"github.com/brutella/hc/accessory"
	"gitlab.com/MikeTTh/env"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"time"
)

type Lamp struct {
	Name      string    `json:"name"`
	LastCheck time.Time `json:"lastCheck"`
	IsOn      bool      `json:"isOn"`
}

type Response struct {
	Type  string  `json:"type"`
	Error string  `json:"error,omitempty"`
	Data  []*Lamp `json:"data,omitempty"`
}

var client http.Client
var cookie http.Cookie

func init() {
	jar, err := cookiejar.New(nil)
	if err != nil {
		log.Fatalf("Got error while creating cookie jar %s", err.Error())
	}
	client = http.Client{
		Jar: jar,
	}
	cookie = http.Cookie{
		Name:  env.String("COOKIENAME", "session"),
		Value: env.String("COOKIEVALUE", "8ae30f5d-02ed-467f-930b-50d92bcda826"),
	}
}

var accessories = make([]*accessory.Accessory, 0)
var lightbulbs = make([]*accessory.Lightbulb, 0)

func main() {
	lamps, err := getLamps()
	if err != nil {
		log.Println(err)
	}

	for i, lamp := range lamps {
		l := lamp
		id := i + 2
		info := accessory.Info{
			Name:         l.Name,
			Manufacturer: "Disappointment Industries",
			ID:           uint64(id),
		}
		acc := accessory.NewLightbulb(info)
		acc.Lightbulb.On.OnValueRemoteUpdate(func(on bool) {
			err := lampSwitch(on)(l.Name)
			if err != nil {
				fmt.Println(err)
			}
		})
		accessories = append(accessories, acc.Accessory)
		lightbulbs = append(lightbulbs, acc)
	}

	go func() {
		for {
			updateLampStatus()
			time.Sleep(10 * time.Second)
		}
	}()

	bridge := accessory.NewBridge(accessory.Info{
		Name:             "Lämp",
		SerialNumber:     "69696969",
		Manufacturer:     "Disappointment Industries",
		Model:            "Lämp",
		FirmwareRevision: "69",
		ID:               1,
	})

	t, err := hc.NewIPTransport(hc.Config{Pin: env.String("HOMEKIT_PIN", "32191123")}, bridge.Accessory, accessories...)
	if err != nil {
		log.Fatal(err)
	}

	hc.OnTermination(func() {
		<-t.Stop()
	})

	t.Start()
}

func getLamps() ([]*Lamp, error) {
	req, _ := http.NewRequest("GET", "https://lamp.mikesweb.site/lamps", nil)
	req.AddCookie(&cookie)

	resp, err := client.Do(req)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

	}

	var result Response
	if err := json.Unmarshal(body, &result); err != nil {
		fmt.Println("Can not unmarshal JSON")
	}

	return result.Data, err
}

func lampSwitch(on bool) func(string) error {
	return func(lamp string) error {
		endpoint := "/off"
		if on {
			endpoint = "/on"
		}
		url := fmt.Sprintf("%s/%s/%s", "https://lamp.mikesweb.site", endpoint, lamp)
		req, _ := http.NewRequest("GET", url, nil)
		req.AddCookie(&cookie)
		resp, err := client.Do(req)
		if err != nil {
			// TODO
		}

		b, e := ioutil.ReadAll(resp.Body)
		if e != nil {
			return e
		}

		if resp.StatusCode != 200 {
			return errors.New(string(b))
		}
		return nil
	}
}

func updateLampStatus() {
	lamps, err := getLamps()
	if err != nil {
		log.Println(err)
	}

	for _, lamp := range lamps {
		var acc *accessory.Lightbulb
		for _, a := range lightbulbs {
			if a.Info.Name.String.GetValue() == lamp.Name {
				acc = a
				break
			}
		}
		if acc == nil {
			continue
		}

		acc.Lightbulb.On.SetValue(lamp.IsOn)
	}
}
