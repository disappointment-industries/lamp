package web

import (
	"gitlab.com/disappointment-industries/lamp/pi/lampmgr"
	"net/http"
)

func OnOffHandler(on bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if len(r.URL.Path) == 0 {
			w.WriteHeader(400)
			_, _ = w.Write([]byte("wtf man"))
			return
		}
		lampname := r.URL.Path[1:]
		for _, l := range lampmgr.GetLamps() {
			if l.Name == lampname {
				var e error
				if on {
					e = l.On()
					_, _ = w.Write([]byte("OK"))
				} else {
					e = l.Off()
					_, _ = w.Write([]byte("OK"))
				}
				if e != nil {
					handleErr(w, e)
					return
				}
				return
			}
		}
		w.WriteHeader(404)
		_, _ = w.Write([]byte("lamp not found"))
	})
}

var HandleOn = OnOffHandler(true)
var HandleOff = OnOffHandler(false)
