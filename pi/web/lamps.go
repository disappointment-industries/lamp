package web

import (
	"encoding/json"
	"gitlab.com/disappointment-industries/lamp/pi/lampmgr"
	"net/http"
)

func HandleLamps(w http.ResponseWriter, r *http.Request) {
	b, e := json.Marshal(lampmgr.GetLamps())
	if e != nil {
		handleErr(w, e)
		return
	}
	_, _ = w.Write(b)
}
