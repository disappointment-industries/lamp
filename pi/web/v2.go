package web

import (
	"encoding/json"
	"gitlab.com/disappointment-industries/lamp/pi/lampmgr"
	"net/http"
	"strings"
)

type ReqData struct {
	Active bool
}

func V2Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodPost {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("invalid request"))
		return
	}

	if r.URL.Query().Get("magic") != magic && r.Header.Get("Authorization") != "Bearer "+magic {
		w.WriteHeader(http.StatusForbidden)
		_, _ = w.Write([]byte("i need your magic"))
		return
	}

	parts := strings.Split(r.URL.Path, "/")
	name := parts[len(parts)-1]
	if name == "" {
		name = parts[len(parts)-2]
	}

	lamps := lampmgr.GetLamps()
	for _, lamp := range lamps {
		if lamp.Name == name {
			switch r.Method {
			case http.MethodGet:
				j, _ := json.Marshal(lamp)
				_, _ = w.Write(j)
			case http.MethodPost:
				var data ReqData
				e := json.NewDecoder(r.Body).Decode(&data)
				if e != nil {
					w.WriteHeader(400)
					_, _ = w.Write([]byte("invalid request"))
					return
				}
				if data.Active {
					e = lamp.On()
				} else {
					e = lamp.Off()
				}
				if e != nil {
					w.WriteHeader(500)
					_, _ = w.Write([]byte(e.Error()))
				}
			}
			return
		}
	}
	w.WriteHeader(404)
	_, _ = w.Write([]byte("not found"))
}
