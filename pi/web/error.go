package web

import (
	"log"
	"net/http"
)

func handleErr(w http.ResponseWriter, e error) {
	log.Println(e)
	w.WriteHeader(500)
	_, _ = w.Write([]byte("internal error"))
}
