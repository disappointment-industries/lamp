package web

import (
	"gitlab.com/MikeTTh/env"
	"net/http"
)

var magic = env.GetOrDosomething("MAGIC", func() string { panic("bring me magic") })

func MagicHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Query().Get("magic") != magic {
			w.WriteHeader(http.StatusForbidden)
			_, _ = w.Write([]byte("i need your magic"))
			return
		}

		handler.ServeHTTP(w, r)
	})
}
