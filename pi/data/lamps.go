package data

import "time"

type Lamp struct {
	Name      string `gorm:"type:varchar(20);primaryKey"`
	IsOn      bool
	Pin       int  `gorm:"-"`
	Invert    bool `gorm:"-"`
	LastCheck time.Time
}

func (l *Lamp) Online() bool {
	return time.Now().Before(l.LastCheck.Add(time.Minute))
}
