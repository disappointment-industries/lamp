package main

import (
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/lamp/pi/web"
	"log"
	"net/http"
)

var listenAddr = env.String("LISTEN", ":8080")

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/v2/", web.V2Handler)
	mux.Handle("/lamps/", web.MagicHandler(http.HandlerFunc(web.HandleLamps)))
	mux.Handle("/on/", web.MagicHandler(http.StripPrefix("/on", web.HandleOn)))
	mux.Handle("/off/", web.MagicHandler(http.StripPrefix("/off", web.HandleOff)))
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("https://gitlab.com/disappointment-industries/lamp"))
	})

	e := http.ListenAndServe(listenAddr, mux)
	if e != nil {
		log.Fatal(e)
	}
}
