package lampmgr

import (
	"github.com/warthog618/gpiod"
	"gitlab.com/disappointment-industries/lamp/pi/data"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"time"
)

const conf = "config/lamps.yml"

var chip *gpiod.Chip

type Lamp struct {
	data.Lamp
	line *gpiod.Line
}

var lamps []*Lamp

func GetLamps() []*Lamp {
	t := time.Now()
	for _, lamp := range lamps {
		lamp.LastCheck = t
	}
	return lamps
}

func init() {
	by, e := ioutil.ReadFile(conf)
	if e != nil {
		panic(e)
	}
	var l []data.Lamp
	e = yaml.Unmarshal(by, &l)
	if e != nil {
		panic(e)
	}

	lamps = make([]*Lamp, len(l))

	for i, la := range l {
		l, e := NewLamp(la)
		if e != nil {
			panic(e)
		}
		lamps[i] = l
	}
}
