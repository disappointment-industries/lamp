// +build !fake

package lampmgr

import (
	"github.com/warthog618/gpiod"
	"gitlab.com/disappointment-industries/lamp/pi/data"
)

func init() {
	var e error
	chip, e = gpiod.NewChip("gpiochip0", gpiod.WithConsumer("softwire"))
	if e != nil {
		panic(e)
	}
}

func (l *Lamp) On() error {
	l.IsOn = true
	val := 1
	if l.Invert {
		val = 0
	}
	return l.line.SetValue(val)
}

func (l *Lamp) Off() error {
	l.IsOn = false
	val := 0
	if l.Invert {
		val = 1
	}
	return l.line.SetValue(val)
}

func NewLamp(l data.Lamp) (*Lamp, error) {
	la := &Lamp{}
	la.Lamp = l
	var e error
	la.line, e = chip.RequestLine(l.Pin, gpiod.AsOutput())
	if e != nil {
		return nil, e
	}
	e = la.On()
	return la, e
}
