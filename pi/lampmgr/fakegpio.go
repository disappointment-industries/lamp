// +build fake

package lampmgr

import (
	"fmt"
	"gitlab.com/disappointment-industries/lamp/pi/data"
)

func (l *Lamp) On() error {
	l.IsOn = true
	fmt.Println(l.Name, "turned on")
	return nil
}

func (l *Lamp) Off() error {
	l.IsOn = false
	fmt.Println(l.Name, "turned off")
	return nil
}

func NewLamp(l data.Lamp) (*Lamp, error) {
	la := &Lamp{}
	la.Lamp = l
	return la, nil
}
