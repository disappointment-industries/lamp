#!/bin/bash

set -o allexport
source secret.env
set +o allexport

export GCP=$(cat ../gcp-key.json)

envsubst < values.yaml | helm upgrade lamp . --values - "$@"
