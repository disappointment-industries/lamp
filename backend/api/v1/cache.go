package v1

import "gitlab.com/disappointment-industries/lamp/pi/data"

func getLampFromCache(name string) (*CachedLamp, error) {
	l := &CachedLamp{}
	e := db.First(l, "name = ?", name).Error
	return l, e
}

type CachedLamp struct {
	data.Lamp
	Location string
}

func (c *CachedLamp) save() error {
	return db.Save(c).Error
}

func getAllLampsFromCache() ([]*CachedLamp, error) {
	l := make([]*CachedLamp, 0)
	e := db.Order("location, name").Find(&l).Error
	return l, e
}
