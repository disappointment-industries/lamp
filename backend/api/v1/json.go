package v1

import (
	"encoding/json"
	"net/http"
	"time"
)

type Lamp struct {
	Name      string    `json:"name"`
	LastCheck time.Time `json:"lastCheck"`
	IsOn      bool      `json:"isOn"`
}

type Response struct {
	Type  string  `json:"type"`
	Error string  `json:"error,omitempty"`
	Data  []*Lamp `json:"data,omitempty"`
}

func JsonHandler(w http.ResponseWriter, r *http.Request) {
	lamps, _ := GetLamps()
	if lamps == nil {
		by, _ := json.Marshal(Response{
			Type:  "error",
			Error: "Couldn't load lamps",
		})
		_, _ = w.Write(by)
		return
	}

	resp := make([]*Lamp, 0)
	for _, lamp := range lamps {
		resp = append(resp, &Lamp{
			Name:      lamp.Name,
			LastCheck: lamp.LastCheck,
			IsOn:      lamp.IsOn,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(Response{
		Type: "success",
		Data: resp,
	})
}
