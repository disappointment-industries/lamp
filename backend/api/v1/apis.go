package v1

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/disappointment-industries/lamp/backend/metrics"
	"gitlab.com/disappointment-industries/lamp/pi/data"
	"io/ioutil"
	"net/http"
	"sync"
)

func getOneLamp(location string) ([]data.Lamp, error) {
	url := fmt.Sprintf("%s/lamps/?magic=%s", location, magic)
	r, e := http.Get(url)
	if e != nil {
		return nil, e
	}

	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		return nil, e
	}

	var l []data.Lamp
	e = json.Unmarshal(b, &l)
	if e != nil {
		return nil, e
	}

	return l, nil
}

func GetLamps() ([]data.Lamp, error) {
	lamps, err := getAllLampsFromCache()

	var wg sync.WaitGroup
	for _, loc := range locations {
		wg.Add(1)
		go func(loc string) {
			l, e := getOneLamp(loc)
			for _, lamp := range l {
				go metrics.LampState(lamp.Name, lamp.IsOn)
				la := &CachedLamp{
					Lamp:     lamp,
					Location: loc,
				}
				e = la.save()
				if e != nil {
					err = e
				}
				for i, cl := range lamps {
					if cl.Name == la.Name {
						lamps[i] = la
					}
				}
			}
			if e != nil {
				err = e
			}
			wg.Done()
		}(loc)
	}

	wg.Wait()

	ret := make([]data.Lamp, 0)

	for _, v := range lamps {
		ret = append(ret, v.Lamp)
	}

	return ret, err
}

func LampSwitch(on bool) func(string) error {
	return func(lamp string) error {
		endpoint := "/off"
		if on {
			endpoint = "/on"
		}
		if l, err := getLampFromCache(lamp); err != nil {
			return err
		} else {
			url := fmt.Sprintf("%s/%s/%s?magic=%s", l.Location, endpoint, lamp, magic)
			r, e := http.Get(url)
			if e != nil {
				return e
			}

			b, e := ioutil.ReadAll(r.Body)
			if e != nil {
				return e
			}

			if r.StatusCode != 200 {
				return errors.New(string(b))
			}

			go metrics.LampSwitch(lamp)
			return nil
		}
	}
}

var LampOn = LampSwitch(true)
var LampOff = LampSwitch(false)
