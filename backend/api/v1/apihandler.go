package v1

import (
	"gitlab.com/disappointment-industries/lamp/backend/errorpage"
	"log"
	"net/http"
	"strings"
)

func OnOffHandler(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 {
		errorpage.ErrorPage(w, r, "no, just no")
		return
	}

	operation := parts[1]
	lamp := parts[2]

	var e error
	switch operation {
	case "on":
		e = LampOn(lamp)
	case "off":
		e = LampOff(lamp)
	default:
		errorpage.ErrorPage(w, r, "no, just no")
	}
	if e != nil {
		errorpage.ErrorPage(w, r, "F")
		log.Println(e)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
