package v1

import (
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
	"net/http"
	"strings"
	"time"
)

var locations = strings.Split(env.String("PI", "http://lampa.internal:8080,http://ff.internal:8080"), ",")
var magic = env.GetOrDosomething("MAGIC", func() string { panic("magic pls") })

func init() {
	for i := range locations {
		locations[i] = strings.TrimSpace(locations[i])
	}
	if len(locations) == 1 && locations[0] == "" {
		locations = make([]string, 0)
	}

	// keep-alive
	go func() {
		for {
			_, _ = GetLamps()
			time.Sleep(10 * time.Second)
		}
	}()
}

func init() {
	http.DefaultClient.Timeout = 2 * time.Second
}

var db = auth.GetDB()

func init() {
	e := db.AutoMigrate(&CachedLamp{})
	if e != nil {
		panic(e)
	}
}
