package homepage

//go:generate ../static/magic.sh

import (
	"fmt"
	"gitlab.com/disappointment-industries/lamp/backend/api/v1"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
	"gitlab.com/disappointment-industries/lamp/pi/data"
	"html/template"
	"log"
	"net/http"
)

type home struct {
	Lamps    []data.Lamp
	Username string
}

var templ *template.Template

func init() {
	var err error
	templ, err = template.ParseFiles("homepage/index.template.html")
	if err != nil {
		panic(err)
	}
}

func Handler(w http.ResponseWriter, r *http.Request) {
	l, e := v1.GetLamps()
	if e != nil {
		log.Println(e)
	}
	d := &home{
		Username: auth.GetUsername(r),
		Lamps:    l,
	}

	err := templ.Execute(w, d)
	if err != nil {
		fmt.Println(err)
	}
}
