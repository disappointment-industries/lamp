package main

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/MikeTTh/graceful"
	"gitlab.com/disappointment-industries/lamp/backend/api/v1"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
	google_home "gitlab.com/disappointment-industries/lamp/backend/google-home"
	"gitlab.com/disappointment-industries/lamp/backend/health"
	"gitlab.com/disappointment-industries/lamp/backend/homepage"
	"log"
	"net/http"
	"os"
	"syscall"
)

var listenAddr = env.String("LISTEN", ":8080")

func init() {
	graceful.AddHook(func(signal os.Signal, done func()) {
		log.Println("Signal", signal.String(), "received")
		log.Println("Stopping lamp backend")
		done()
	})
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	defer graceful.RunHooks()
	graceful.Listen(
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGTERM,
		syscall.SIGSTOP,
	)

	authMux := http.NewServeMux()
	authMux.HandleFunc("/on/", v1.OnOffHandler)
	authMux.HandleFunc("/off/", v1.OnOffHandler)
	authMux.HandleFunc("/logout/", auth.LogoutHandler)
	authMux.HandleFunc("/app_auth", auth.AppAuthHandler)

	mux := http.NewServeMux()
	mux.Handle("/static/", http.FileServer(http.Dir(".")))
	mux.HandleFunc("/login", auth.LoginHandler)

	mux.HandleFunc("/authorize", google_home.AuthHandler)
	mux.HandleFunc("/token", google_home.TokenHandler)
	mux.HandleFunc("/google-api", google_home.GoogleApiHandler)
	mux.HandleFunc("/lamps", v1.JsonHandler)

	authHndlr := auth.NeedsAuth(authMux)
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			homepage.Handler(w, r)
		} else {
			authHndlr.ServeHTTP(w, r)
		}
	})

	go func() {
		e := http.ListenAndServe(env.String("HEALTHCHECK", ":8000"), health.Health)
		if e != nil {
			panic(e)
		}
	}()

	go func() {
		e := http.ListenAndServe(env.String("METRICS", ":8001"), promhttp.Handler())
		if e != nil {
			panic(e)
		}
	}()

	log.Println("Started lamp backend on", listenAddr)
	e := http.ListenAndServe(listenAddr, mux)
	if e != nil {
		log.Println(e)
	}
}
