package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const prefix = "lamp_"
const labelName = "device"

var states = promauto.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: prefix + "state",
		Help: "State of lamps currently",
	},
	[]string{labelName},
)

var switches = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: prefix + "switches",
		Help: "Number of toggles",
	},
	[]string{labelName},
)

func l(name string) prometheus.Labels {
	return prometheus.Labels{
		labelName: name,
	}
}

func LampSwitch(name string) {
	switches.With(l(name)).Add(1.0)
}

func LampState(name string, state bool) {
	s := 0.0
	if state {
		s = 1.0
	}
	states.With(l(name)).Set(s)
}
