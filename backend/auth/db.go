package auth

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/MikeTTh/env"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"time"
)

type cook struct {
	Id      uuid.UUID `gorm:"type:uuid;default:gen_random_uuid()"`
	Name    string    `gorm:"type:varchar(50)"`
	Created time.Time `gorm:"type:timestamptz;default:now()"`
}

var db *gorm.DB

func init() {
	e := retry(func() error {
		var e error
		db, e = gorm.Open(
			postgres.Open(env.String("POSTGRES", "postgresql://postgres:postgres@localhost/lamp")),
			&gorm.Config{
				PrepareStmt: true,
			},
		)
		if e != nil {
			return e
		}
		return db.AutoMigrate(&cook{})
	})
	if e != nil {
		panic(e)
	}
}

func retry(f func() error) error {
	var e error
	for i := 0; i < 3; i++ {
		e = f()
		if e != nil {
			fmt.Println(e)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	return e
}

func AddCookie(user string) (string, error) {
	c := cook{
		Name: user,
	}
	e := db.Save(&c).Error
	if e != nil {
		return "", e
	}
	return c.Id.String(), nil
}

func GetCookie(i string) string {
	u, e := uuid.Parse(i)
	if e != nil {
		return ""
	}
	c := cook{
		Id: u,
	}
	e = db.First(&c).Error
	if e != nil {
		return ""
	}
	return c.Name
}

func DelCookie(i string) error {
	u, e := uuid.Parse(i)
	if e != nil {
		return e
	}
	c := cook{
		Id: u,
	}
	e = db.Delete(&c).Error
	return e
}

func GetDB() *gorm.DB {
	return db
}
