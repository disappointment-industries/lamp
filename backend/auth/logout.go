package auth

import (
	"gitlab.com/disappointment-industries/lamp/backend/errorpage"
	"log"
	"net/http"
)

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	c, e := r.Cookie(cookieName)
	if e != nil {
		errorpage.ErrorPage(w, r, "couldn't log you out")
		log.Println(e)
		return
	}
	e = DelCookie(c.Value)
	if e != nil {
		errorpage.ErrorPage(w, r, "couldn't log you out")
		log.Println(e)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
