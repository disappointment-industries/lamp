package auth

import (
	"fmt"
	"git.sch.bme.hu/kszk/opensource/authsch-go"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/lamp/backend/errorpage"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const cookieName = "session"

func plspanic() string { panic("CLIENTID or CLIENTSECRET env var is missing") }

var allowedUsers = strings.Split(env.String("USERS", "mikewashere,blintmester"), ",")

var auth authsch.Client

var noauth = env.Bool("NOAUTH", false)

func init() {
	if noauth {
		return
	}

	auth = authsch.CreateClient(
		env.GetOrDosomething("CLIENTID", plspanic),
		env.GetOrDosomething("CLIENTSECRET", plspanic),
		[]string{
			"basic",
			"linkedAccounts",
		},
	)
}

func GetUsername(r *http.Request) string {
	cook, e := r.Cookie(cookieName)
	if e != nil {
		return ""
	}
	return GetCookie(cook.Value)
}

var loginHandler = auth.GetLoginHandler(
	func(details *authsch.AccDetails, w http.ResponseWriter, r *http.Request) {
		for _, u := range allowedUsers {
			if u == details.LinkedAccounts.SchAcc {
				c, e := AddCookie(u)
				if e != nil {
					errorpage.ErrorPage(w, r, "we don't know what we are doing")
					log.Println(e)
					return
				}
				cook := http.Cookie{
					Name:    cookieName,
					Value:   c,
					Path:    "/",
					Expires: time.Now().AddDate(1, 0, 0),
				}
				http.SetCookie(w, &cook)
				red, _ := r.Cookie("redir")
				if red == nil || red.Value == "" {
					http.Redirect(w, r, "/", http.StatusFound)
					return
				}
				delCook(red)
				http.SetCookie(w, red)
				http.Redirect(w, r, red.Value, http.StatusFound)
			}
		}
		errorpage.ErrorPage(w, r, "You aren't allowed to use this service")
	},
	func(w http.ResponseWriter, r *http.Request) {
		errorpage.ErrorPage(w, r, "An error happened trying to log you in")
	},
)

func noauthAuther(w http.ResponseWriter, r *http.Request) {
	ra := rand.Intn(200)
	c, e := AddCookie(fmt.Sprintf("user%d", ra))
	if e != nil {
		errorpage.ErrorPage(w, r, "we don't know what we are doing")
		log.Println(e)
		return
	}
	cook := http.Cookie{
		Name:    cookieName,
		Value:   c,
		Path:    "/",
		Expires: time.Now().AddDate(1, 0, 0),
	}
	http.SetCookie(w, &cook)
	red, _ := r.Cookie("redir")
	if red == nil || red.Value == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	delCook(red)
	http.SetCookie(w, red)
	http.Redirect(w, r, red.Value, http.StatusFound)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	if noauth {
		noauthAuther(w, r)
		return
	}

	if r.URL.Path == "/login" && !r.URL.Query().Has("code") {
		http.Redirect(w, r, auth.GetAuthURL(), http.StatusFound)
		return
	}

	loginHandler.ServeHTTP(w, r)
}

func AppAuthHandler(w http.ResponseWriter, r *http.Request) {
	c, e := r.Cookie(cookieName)
	if e != nil {
		errorpage.ErrorPage(w, r, e.Error())
		return
	}

	http.Redirect(
		w,
		r,
		fmt.Sprintf("lamp://login?id=%s&name=%s&expires=%d", c.Value, GetUsername(r), c.Expires.Unix()),
		http.StatusFound,
	)
}

func redirCook(redir string) *http.Cookie {
	return &http.Cookie{
		Name:    "redir",
		Value:   redir,
		Path:    "/",
		Expires: time.Now().AddDate(0, 0, 1),
	}
}

func delCook(c *http.Cookie) {
	c.Expires = time.Now()
}

func NeedsAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if GetUsername(r) == "" {
			path := r.URL.String()
			http.SetCookie(w, redirCook(path))
			http.Redirect(w, r, fmt.Sprintf("/login"), http.StatusFound)
			return
		}
		h.ServeHTTP(w, r)
	})
}
