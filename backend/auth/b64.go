package auth

import "encoding/base64"

func encode(s string) string {
	sEnc := base64.URLEncoding.EncodeToString([]byte(s))
	return sEnc
}

func decode(s string) (string, error) {
	sDec, e := base64.URLEncoding.DecodeString(s)
	if e != nil {
		return "", e
	}
	return string(sDec), nil
}
