package errorpage

import "net/http"

func ErrorPage(w http.ResponseWriter, r *http.Request, error string) {
	_, _ = w.Write([]byte("whoops " + error))
}
