package health

import (
	"github.com/heptiolabs/healthcheck"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
	"time"
)

var Health = healthcheck.NewHandler()

func init() {
	db, e := auth.GetDB().DB()
	if e == nil {
		Health.AddReadinessCheck("db", healthcheck.DatabasePingCheck(db, time.Second))
	}
}
