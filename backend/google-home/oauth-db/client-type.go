package oauth_db

type Client struct {
	ID     string `gorm:"type:uuid;default:gen_random_uuid()"`
	Secret string `gorm:"type:varchar(50)"`
	Domain string `gorm:"type:varchar(50)"`
	UserID string `gorm:"type:varchar(50)"`
}

func (c *Client) GetID() string {
	return c.ID
}

func (c *Client) GetSecret() string {
	return c.Secret
}

func (c *Client) GetDomain() string {
	return c.Domain
}

func (c *Client) GetUserID() string {
	return c.UserID
}
