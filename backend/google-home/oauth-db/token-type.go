package oauth_db

import (
	"database/sql"
	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/jinzhu/copier"
	"log"
	"reflect"
	"time"
)

type Token struct {
	ClientID            string         `gorm:"type:uuid"`
	UserID              string         `gorm:"type:varchar(50)"`
	RedirectURI         string         `gorm:"type:varchar(100)"`
	Scope               string         `gorm:"type:varchar(10)"`
	Code                sql.NullString `gorm:"type:varchar(512);uniqueIndex"`
	CodeChallenge       sql.NullString `gorm:"type:varchar(20)"`
	CodeChallengeMethod sql.NullString `gorm:"type:varchar(20)"`
	CodeCreateAt        time.Time      `gorm:"type:timestamptz"`
	CodeExpiresIn       time.Duration
	Access              sql.NullString `gorm:"type:varchar(512);uniqueIndex"`
	AccessCreateAt      time.Time      `gorm:"type:timestamptz"`
	AccessExpiresIn     time.Duration
	Refresh             sql.NullString `gorm:"type:varchar(512);uniqueIndex"`
	RefreshCreateAt     time.Time      `gorm:"type:timestamptz"`
	RefreshExpiresIn    time.Duration
}

func (t *Token) ToTokenInfo() oauth2.TokenInfo {
	to := &models.Token{}
	e := copier.Copy(to, t)
	if e != nil {
		log.Println(e)
		return nil
	}
	return to
}

func (t *Token) FromToken(tok *models.Token) error {
	e := copier.Copy(t, tok)
	if e != nil {
		return e
	}

	nulStrType := reflect.TypeOf(sql.NullString{})

	v := reflect.ValueOf(t).Elem()
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		typ := f.Type()
		if typ == nulStrType {
			ns := f.Interface().(sql.NullString)
			if ns.String == "" {
				ns.Valid = false
				f.Set(reflect.ValueOf(ns))
			}
		}
	}

	return nil
}
