package oauth_db

import (
	"context"
	"errors"
	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/models"
)

var TypeError = errors.New("invalid type")

type TokenStore struct {
	oauth2.TokenStore
}

func (t *TokenStore) Create(ctx context.Context, info oauth2.TokenInfo) error {
	src, ok := info.(*models.Token)
	if !ok {
		return TypeError
	}

	to := &Token{}
	e := to.FromToken(src)
	if e != nil {
		return e
	}

	return db.Create(&to).Error
}

func (t *TokenStore) RemoveByCode(ctx context.Context, code string) error {
	return db.Delete(&Token{}, "code = ?", code).Error
}

func (t *TokenStore) RemoveByAccess(ctx context.Context, access string) error {
	return db.Delete(&Token{}, "access = ?", access).Error
}

func (t *TokenStore) RemoveByRefresh(ctx context.Context, refresh string) error {
	return db.Delete(&Token{}, "refresh = ?", refresh).Error
}

func (t *TokenStore) GetByCode(ctx context.Context, code string) (oauth2.TokenInfo, error) {
	to := &Token{}
	e := db.First(to, "code = ?", code).Error
	return to.ToTokenInfo(), e
}

func (t *TokenStore) GetByAccess(ctx context.Context, access string) (oauth2.TokenInfo, error) {
	to := &Token{}
	e := db.First(to, "access = ?", access).Error
	return to.ToTokenInfo(), e
}

func (t *TokenStore) GetByRefresh(ctx context.Context, refresh string) (oauth2.TokenInfo, error) {
	to := &Token{}
	e := db.First(to, "refresh = ?", refresh).Error
	return to.ToTokenInfo(), e
}

func (t *TokenStore) GetAll(ctx context.Context) ([]*Token, error) {
	var to []*Token
	e := db.Find(&to).Error
	return to, e
}
