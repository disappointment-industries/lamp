package oauth_db

import (
	"context"
	"github.com/go-oauth2/oauth2/v4"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
)

type ClientStore struct {
	oauth2.ClientStore
}

var db = auth.GetDB()

func init() {
	e := db.AutoMigrate(&Client{}, &Token{})
	if e != nil {
		panic(e)
	}
}

func (c *ClientStore) GetByID(ctx context.Context, id string) (oauth2.ClientInfo, error) {
	cl := &Client{
		ID: id,
	}
	e := db.First(cl).Error
	return cl, e
}
