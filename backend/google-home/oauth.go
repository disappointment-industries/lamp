package google_home

import (
	"context"
	"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/server"
	"gitlab.com/disappointment-industries/lamp/backend/google-home/oauth-db"
	"log"
	"net/http"
	"strings"
)

var srv *server.Server

var AuthHandler http.HandlerFunc
var TokenHandler http.HandlerFunc

var store = &oauth_db.TokenStore{}

func init() {
	manager := manage.NewDefaultManager()
	manager.MustTokenStorage(store, nil)
	manager.MapClientStorage(&oauth_db.ClientStore{})

	srv = server.NewDefaultServer(manager)
	srv.SetAllowGetAccessRequest(true)
	srv.SetClientInfoHandler(server.ClientFormHandler)

	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})

	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})

	srv.UserAuthorizationHandler = authHandler

	AuthHandler = func(w http.ResponseWriter, r *http.Request) {
		err := srv.HandleAuthorizeRequest(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}
	TokenHandler = func(w http.ResponseWriter, r *http.Request) {
		e := srv.HandleTokenRequest(w, r)
		if e != nil {
			http.Error(w, e.Error(), http.StatusBadRequest)
		}
	}
}

func getUser(r *http.Request) string {
	header := r.Header.Get("Authorization")
	if !strings.Contains(header, "Bearer") {
		return ""
	}

	header = strings.ReplaceAll(header, "Bearer", "")
	header = strings.TrimSpace(header)

	tok, e := store.GetByAccess(context.Background(), header)
	if e != nil {
		return ""
	}

	return tok.GetUserID()
}
