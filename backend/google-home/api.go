package google_home

import (
	"encoding/json"
	"fmt"
	v1 "gitlab.com/disappointment-industries/lamp/backend/api/v1"
	"gitlab.com/disappointment-industries/lamp/pi/data"
	"log"
	"net/http"
)

func tryUnmarshal(dst interface{}, r *http.Request) error {
	dec := json.NewDecoder(r.Body)
	return dec.Decode(dst)
}

func handleSync(w http.ResponseWriter, r *http.Request, input *Input, id, user string) {
	dev := make([]SyncRespDevice, 0)
	l, e := v1.GetLamps()
	if e != nil {
		log.Println(e)
	}
	for _, lamp := range l {
		dev = append(dev, SyncRespDevice{
			Id:   lamp.Name,
			Type: "action.devices.types.LIGHT",
			Traits: []string{
				"action.devices.traits.OnOff",
			},
			Name: Name{
				Name: lamp.Name,
			},
			DeviceInfo: Info{
				Manufacturer: "Disappointment Industries",
				Model:        "Lämp",
				HwVersion:    "v0.0.0",
				SwVersion:    "v0.0.0",
			},
		})
	}

	var resp = syncResponse{
		RequestId: id,
		Payload: SyncRespPayload{
			AgentUserId: user,
			Devices:     dev,
		},
	}
	e = json.NewEncoder(w).Encode(resp)
	if e != nil {
		log.Println(e)
	}
}

func handleQuery(w http.ResponseWriter, r *http.Request, input *Input, id, user string) {
	d := make(map[string]QueryRespDevice)
	l, e := v1.GetLamps()
	if e != nil {
		log.Println(e)
	}
	for _, dev := range input.Payload.Devices {
		var lamp data.Lamp
		found := false
		for _, la := range l {
			if dev.Id == la.Name {
				lamp = la
				found = true
				break
			}
		}

		d[lamp.Name] = QueryRespDevice{
			On:     lamp.IsOn,
			Online: found && lamp.Online(),
		}
	}

	var resp = queryResponse{
		RequestId: id,
		Payload: QueryRespPayload{
			AgentUserId: user,
			Devices:     d,
		},
	}

	e = json.NewEncoder(w).Encode(resp)
	if e != nil {
		log.Println(e)
	}
}

func handleExecute(w http.ResponseWriter, r *http.Request, input *Input, id, user string) {
	var resp = execResponse{
		RequestId: id,
	}

	for _, cmd := range input.Payload.Commands {
		OnOff := false
		do := false
		for _, exec := range cmd.Execution {
			if exec.Command == "action.devices.commands.OnOff" {
				do = true
			}
			OnOff = exec.Params.On
		}
		if do {
			f := v1.LampSwitch(OnOff)
			for _, device := range cmd.Devices {
				stat := "SUCCESS"
				errorCode := ""
				e := f(device.Id)
				if e != nil {
					log.Println(e)
					stat = "ERROR"
					errorCode = e.Error()
				}
				resp.Payload.Commands = append(resp.Payload.Commands, CmdResp{
					Ids:    []string{device.Id},
					Status: stat,
					States: State{
						On:     OnOff,
						Online: e == nil,
					},
					ErrorCode: errorCode,
				})
			}
		}
	}

	e := json.NewEncoder(w).Encode(resp)
	if e != nil {
		log.Println(e)
	}
}

func GoogleApiHandler(w http.ResponseWriter, r *http.Request) {
	user := getUser(r)
	if user == "" {
		return
	}

	var q req
	e := tryUnmarshal(&q, r)
	if e != nil {
		fmt.Println(e)
	}

	for idx := range q.Inputs {
		i := &q.Inputs[idx]
		switch i.Intent {
		case "action.devices.SYNC":
			handleSync(w, r, i, q.RequestId, user)
		case "action.devices.QUERY":
			handleQuery(w, r, i, q.RequestId, user)
		case "action.devices.EXECUTE":
			handleExecute(w, r, i, q.RequestId, user)
		}
	}
}
