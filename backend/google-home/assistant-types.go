package google_home

type Input struct {
	Intent  string     `json:"intent"`
	Payload ReqPayload `json:"payload"`
}

type ReqPayload struct {
	Devices  []ReqDevice `json:"devices"`
	Commands []Command   `json:"commands"`
}

type Command struct {
	Devices   []CmdDevice `json:"devices"`
	Execution []Exec      `json:"execution"`
}

type CmdDevice struct {
	Id string `json:"id"`
}

type Exec struct {
	Command string `json:"command"`
	Params  struct {
		On bool `json:"on"`
	} `json:"params"`
}

type ReqDevice struct {
	Id string `json:"id"`
}

type req struct {
	RequestId string  `json:"requestId"`
	Inputs    []Input `json:"inputs"`
}

type syncResponse struct {
	RequestId string          `json:"requestId"`
	Payload   SyncRespPayload `json:"payload"`
}

type SyncRespPayload struct {
	AgentUserId string           `json:"agentUserId"`
	Devices     []SyncRespDevice `json:"devices"`
}

type SyncRespDevice struct {
	Id              string   `json:"id"`
	Type            string   `json:"type"`
	Traits          []string `json:"traits"`
	Name            Name     `json:"name"`
	WillReportState bool     `json:"willReportState"`
	RoomHint        string   `json:"roomHint"`
	DeviceInfo      Info     `json:"deviceInfo"`
}

type Name struct {
	DefaultNames []string `json:"defaultNames"`
	Name         string   `json:"name"`
	Nicknames    []string `json:"nicknames"`
}

type Info struct {
	Manufacturer string `json:"manufacturer"`
	Model        string `json:"model"`
	HwVersion    string `json:"hwVersion"`
	SwVersion    string `json:"swVersion"`
}

type queryResponse struct {
	RequestId string           `json:"requestId"`
	Payload   QueryRespPayload `json:"payload"`
}

type QueryRespPayload struct {
	AgentUserId string                     `json:"agentUserId"`
	Devices     map[string]QueryRespDevice `json:"devices"`
}

type QueryRespDevice struct {
	On     bool `json:"on"`
	Online bool `json:"online"`
}

type execResponse struct {
	RequestId string      `json:"requestId"`
	Payload   ExecPayload `json:"payload"`
}

type ExecPayload struct {
	Commands []CmdResp `json:"commands"`
}

type CmdResp struct {
	Ids       []string `json:"ids"`
	Status    string   `json:"status"`
	States    State    `json:"states,omitempty"`
	ErrorCode string   `json:"errorCode,omitempty"`
}

type State struct {
	On     bool `json:"on"`
	Online bool `json:"online"`
}
