package google_home

import (
	"github.com/go-oauth2/oauth2/v4/errors"
	"gitlab.com/disappointment-industries/lamp/backend/auth"
	"net/http"
)

func authHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	username := auth.GetUsername(r)
	if username == "" {
		return "", errors.ErrAccessDenied
	}
	return username, nil
}
