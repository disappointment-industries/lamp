package google_home

import (
	"context"
	"google.golang.org/api/homegraph/v1"
	"log"
)

func init() {
	go func() {
		ctx := context.Background()
		homegraphService, err := homegraph.NewService(ctx)
		if err != nil {
			panic(err)
		}
		tok, e := store.GetAll(ctx)
		if e != nil {
			panic(e)
		}
		for _, token := range tok {
			c := homegraphService.Devices.RequestSync(&homegraph.RequestSyncDevicesRequest{
				AgentUserId: token.UserID,
				Async:       false,
			})
			_, e = c.Do()
			if e != nil {
				log.Println(e)
			}
		}
	}()
}
