#!/bin/bash

cd "$(dirname "$0")" || exit 1

mkdir -p images

for size in "16" "32" "64" "180" "192" "512"
do
  convert moth.png -resize ${size}x${size} images/moth_${size}.png
done