module gitlab.com/disappointment-industries/lamp

go 1.15

replace gitlab.com/disappointment-industries/lamp => ./

require (
	git.sch.bme.hu/kszk/opensource/authsch-go v0.0.0-20220512182944-b430d75f76f0
	github.com/brutella/hc v1.2.4
	github.com/go-oauth2/oauth2/v4 v4.4.2
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.3.0
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/jinzhu/copier v0.3.2
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3 // indirect
	github.com/prometheus/client_golang v1.12.1
	github.com/tidwall/btree v0.6.1 // indirect
	github.com/tidwall/buntdb v1.2.6 // indirect
	github.com/tidwall/gjson v1.9.4 // indirect
	github.com/warthog618/gpiod v0.6.0
	gitlab.com/MikeTTh/env v0.0.0-20210102155928-2e9be3823cc7
	gitlab.com/MikeTTh/graceful v0.0.0-20210107133402-2d7e6b66efd9
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5 // indirect
	google.golang.org/api v0.58.0
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.16
)
