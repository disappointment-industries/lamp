package site.mikesweb.lamp.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import site.mikesweb.lamp.R
import site.mikesweb.lamp.helpers.toast
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.LampState

@Composable
fun MainView() {
    val context = LocalContext.current
    val client = LocalClient.current
    val scope = rememberCoroutineScope()
    val lamps = LocalLamps.current
    val groups = LocalLampGroups.current
    val selected = remember { mutableStateListOf<Lamp>() }
    val loaded = LocalLampsLoaded.current

    fun toggleSelect(lamp: Lamp) = if (selected.contains(lamp)) {
        selected -= lamp
    } else {
        selected += lamp
    }

    var dialogVisible by remember { mutableStateOf(false) }

    Crossfade(loaded) { load ->
        if (load) {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                contentPadding = PaddingValues(16.dp),
            ){
                item {
                    AnimatedVisibility(
                        visible = selected.isNotEmpty(),
                        enter = expandVertically( expandFrom = Alignment.Top ),
                        exit = shrinkVertically( shrinkTowards = Alignment.Top ),
                    ) {
                        Button(onClick = { dialogVisible = true }) {
                            Text(stringResource(R.string.add_group))
                        }
                    }

                }

                items(
                    items = groups,
                    key = { it.name }
                ) {
                    Box(modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentSize(Alignment.TopStart)) {
                        var dropdown by remember { mutableStateOf(false) }
                        LämpCard(
                            lamp = it,
                            click = {
                                scope.launch {
                                    try {
                                        it.toggle(client)
                                    } catch (e: Exception) {
                                        e.message?.let { context.toast(it) }
                                        e.printStackTrace()
                                    }
                                }
                            },
                            longClick = {
                                dropdown = true
                            },
                        )
                        LampGroupDropdown(
                            expanded = dropdown,
                            name = it.name,
                            dismiss = { dropdown = false },
                        )
                    }
                }

                items(
                    items = lamps,
                    key = { it.name },
                ) {
                    LämpCard(
                        lamp = it,
                        selected = selected.contains(it),
                        longClick = { toggleSelect(it) },
                        click = {
                            if (selected.isNotEmpty()) {
                                toggleSelect(it)
                                return@LämpCard
                            }
                            scope.launch {
                                try {
                                    it.toggle(client)
                                    val idx = lamps.indexOf(it)
                                    if (idx < 0) return@launch
                                    val nextState = when (it.state) {
                                        LampState.Off -> LampState.On
                                        LampState.On -> LampState.Off
                                        LampState.LostInSpace -> LampState.LostInSpace
                                    }
                                    lamps[idx] = it.copy(state = nextState)
                                } catch (e: Exception) {
                                    e.message?.let { context.toast(it) }
                                    e.printStackTrace()
                                }
                            }
                        },
                    )
                }
            }
        } else {
            Loading()
        }

    }


    AddDialog(
        visible = dialogVisible,
        dismiss = { dialogVisible = false },
        selected = selected,
    )
}