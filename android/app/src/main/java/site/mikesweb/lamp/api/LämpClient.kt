package site.mikesweb.lamp.api

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.date.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlin.Error

class LämpClient(
    id: String? = null,
    expires: Long? = null,
) {
    companion object {
        const val domain = "lamp.mikesweb.site"
        const val baseUrl = "https://$domain"
    }

    private var http = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(HttpCookies) {
            if (id != null && expires != null) {
                storage = ConstantCookiesStorage(
                    Cookie(
                        name = "session",
                        value = id,
                        domain = domain,
                        expires = GMTDate(expires),
                        path = "/",
                        httpOnly = true,
                        encoding = CookieEncoding.RAW,
                    )
                )
            }
        }
    }

    suspend fun getLamps(): Response = withContext(Dispatchers.IO) {
        val resp = http.get("$baseUrl/lamps")

        if (resp.status != HttpStatusCode.OK) throw Exception("Received status: ${resp.status}")

        resp.body()
    }

    private suspend fun switchLamp(name: String, state: String) = withContext(Dispatchers.IO) {
        val resp = http.get("$baseUrl/$state/${name.encodeURLPath()}")
        if (resp.status != HttpStatusCode.OK) {
            throw Exception("Error turning $name $state: code is ${resp.status}")
        }
        if (resp.request.url.fullPath.contains("login")) {
            throw Exception("You aren't logged in")
        }
    }

    suspend fun lampOn(name: String) = switchLamp(name, "on")
    suspend fun lampOff(name: String) = switchLamp(name, "off")

}