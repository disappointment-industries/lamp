package site.mikesweb.lamp.widget.config

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.glance.GlanceId
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.GlanceAppWidgetReceiver
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.state.PreferencesGlanceStateDefinition
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import site.mikesweb.lamp.composables.*
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.LampGroup
import site.mikesweb.lamp.model.Toggleable
import site.mikesweb.lamp.ui.theme.LämpTheme
import site.mikesweb.lamp.widget.Widget
import site.mikesweb.lamp.widget.Widget.Companion.TYPE_LAMP
import site.mikesweb.lamp.widget.Widget.Companion.TYPE_LAMPGROUP

val LocalWidgetId = compositionLocalOf<Int> { error("WidgetID not provided") }
val LocalGlanceId = compositionLocalOf<GlanceId?> { error("GlanceID not provided") }

val done: (Toggleable)->Unit @Composable get() {
    // todo configure WorkManager to update widgets

    val id = LocalWidgetId.current
    val glanceId = LocalGlanceId.current
    val activity = LocalContext.current as Activity
    val scope = rememberCoroutineScope()


    return {
        scope.launch {
            updateAppWidgetState(activity, glanceId!!) { state ->
                state[Widget.KEY_NAME] = it.name
                state[Widget.KEY_TYPE] = when (it) {
                    is Lamp -> TYPE_LAMP
                    is LampGroup -> TYPE_LAMPGROUP
                    else -> ""
                }
                state[Widget.KEY_STATE] = it.state.ordinal
                state[Widget.KEY_NOTFOUND] = false
            }

            Widget().update(activity, glanceId)
        }

        val resultValue = Intent().putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)
        activity.setResult(Activity.RESULT_OK, resultValue)
        activity.finish()
    }
}

class ConfiguratorActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setResult(RESULT_CANCELED)
        val widgetId = intent?.extras?.getInt(
            AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID
        ) ?: AppWidgetManager.INVALID_APPWIDGET_ID

        var glanceId by mutableStateOf<GlanceId?>(null)

        lifecycleScope.launch {
            glanceId = GlanceAppWidgetManager(this@ConfiguratorActivity)
                .getGlanceIds(Widget::class.java).lastOrNull()
            if (glanceId == null) {
                finish()
            }
        }

        setContent {
            CompositionLocalProvider(
                LocalWidgetId provides widgetId,
                LocalGlanceId provides glanceId,
            ) {
                LämpProvider {
                    LämpTheme {
                        Scaffold(
                            modifier = Modifier.fillMaxSize(),
                            topBar = { LämpBar() },
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(it),
                            ) {
                                LampList()
                            }
                        }
                    }
                }
            }
        }
    }
}