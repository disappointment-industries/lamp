package site.mikesweb.lamp.model

import kotlinx.serialization.Serializable
import site.mikesweb.lamp.api.LämpClient

interface Toggleable {
    val name: String
    val state: LampState

    suspend fun toggle(client: LämpClient)
}

@Serializable
enum class LampState {
    On, Off, LostInSpace
}

@Serializable
data class Lamp(
    override val name: String,
    override val state: LampState,
): Toggleable {
    override suspend fun toggle(client: LämpClient) = when (state) {
        LampState.On -> client.lampOff(name)
        LampState.Off -> client.lampOn(name)
        LampState.LostInSpace -> throw Exception("Lamp is offline")
    }
}

@Serializable
data class SavedLampGroup(
    val name: String,
    val lampNames: List<String>,
)

data class LampGroup(
    override val name: String,
    val lamps: List<Lamp>,
): Toggleable {
    override val state: LampState get() = when {
        lamps.any { it.state == LampState.LostInSpace } -> LampState.LostInSpace
        lamps.any { it.state == LampState.On } -> LampState.On
        else -> LampState.Off
    }

    override suspend fun toggle(client: LämpClient) = when (state) {
        LampState.On -> lamps.forEach { client.lampOff(it.name) }
        LampState.Off -> lamps.forEach { client.lampOn(it.name) }
        LampState.LostInSpace -> throw Exception("Lamp is offline")
    }
}

fun SavedLampGroup.toLampGroup(lamps: List<Lamp>) =
    LampGroup(
        name = name,
        lamps = lampNames
            .map { name -> lamps.firstOrNull { it.name == name } }
            .filter { it != null }.map { it!! },
    )
