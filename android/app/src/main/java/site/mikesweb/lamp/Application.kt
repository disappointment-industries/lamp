package site.mikesweb.lamp

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Looper
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

class Application: Application() {
    private lateinit var workManager: WorkManager

    override fun onCreate() {
        super.onCreate()

        workManager = WorkManager.getInstance(applicationContext)

        val work = PeriodicWorkRequestBuilder<WidgetWorker>(15, TimeUnit.MINUTES)
            .build()
        workManager.enqueueUniquePeriodicWork(
            "poll",
            ExistingPeriodicWorkPolicy.REPLACE,
            work
        )
    }
}

class BootReceiver: BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) { }
}