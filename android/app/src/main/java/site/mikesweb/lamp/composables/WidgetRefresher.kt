package site.mikesweb.lamp.composables

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import site.mikesweb.lamp.widget.refreshWidgets

@Composable
fun WidgetRefresher() {
    val lamps = LocalLamps.current
    val groups = LocalLampGroups.current
    val context = LocalContext.current

    LaunchedEffect(lamps.toList(), groups.toList()) {
        refreshWidgets(context, lamps, groups)
    }
}