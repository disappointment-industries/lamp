package site.mikesweb.lamp.widget

import android.content.Context
import android.graphics.ColorMatrixColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.appwidget.updateAll
import androidx.work.ListenableWorker
import site.mikesweb.lamp.api.Error
import site.mikesweb.lamp.api.LämpClient
import site.mikesweb.lamp.api.Success
import site.mikesweb.lamp.helpers.longPref
import site.mikesweb.lamp.helpers.sharedPref
import site.mikesweb.lamp.helpers.stringPref
import site.mikesweb.lamp.helpers.toLamp
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.LampGroup
import site.mikesweb.lamp.model.SavedLampGroup
import site.mikesweb.lamp.model.toLampGroup

suspend fun refreshWidgets(
    context: Context,
    lamps: List<Lamp>,
    groups: List<LampGroup>
) {
    val ids = GlanceAppWidgetManager(context)
        .getGlanceIds(Widget::class.java)
    ids.forEach {
        updateAppWidgetState(context, it) { state ->
            val type = state[Widget.KEY_TYPE]
            val name = state[Widget.KEY_NAME]
            val lamp = when (type) {
                Widget.TYPE_LAMP -> lamps.firstOrNull { it.name == name }
                Widget.TYPE_LAMPGROUP -> groups.firstOrNull { it.name == name }
                else -> null
            }
            state[Widget.KEY_STATE] = lamp?.state?.ordinal ?: -1
            state[Widget.KEY_NOTFOUND] = lamp == null
        }
    }
    Widget().updateAll(context)
}

suspend fun Context.getAllLampsAndRefresh() {
    val id by stringPref(this)
    val expires by longPref(this)
    val lampGroups by sharedPref<List<SavedLampGroup>>(this)


    val client = LämpClient(id, expires)
    val resp = client.getLamps()
    val lamps = when (resp) {
        is Error -> error(resp.error)
        is Success -> resp.data.map { it.toLamp() }
    }
    val groups = lampGroups?.map { it.toLampGroup(lamps) } ?: emptyList()

    refreshWidgets(this, lamps, groups)
}