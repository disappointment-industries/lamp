package site.mikesweb.lamp.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

private const val PREF_NAME = "lamp"

private val Context.pref get() =
    getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)!!


/*
 * Kotlin Delegates which saves to and reads from SharedPreferences.
 * The name of the SharedPreferences entry is the name of the variable.
 * A Context needs to be passed, either as a parameter, or if the variable
 * is declared inside a Context, then that Context can be used automatically.
 */
fun stringPref(context: Context): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, String?>> =
    SharedPrefFactory(context, { getString(it, null) }, { k, v -> putString(k, v) })
fun intPref(context: Context): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, Int?>> =
    SharedPrefFactory(context, { getInt(it, 0) }, { k, v -> putInt(k, v) })
fun longPref(context: Context): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, Long?>> =
    SharedPrefFactory(context, { getLong(it, 0) }, { k, v -> putLong(k, v) })

val json by lazy {
    Json {
        ignoreUnknownKeys = true
    }
}

inline fun <reified T> sharedPref(context: Context): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, T?>> =
    SharedPrefFactory(
        context,
        { key ->
            val str = getString(key, null) ?: return@SharedPrefFactory null
            json.decodeFromString(str)
        },
        { k, v ->
            val str = json.encodeToString(v)
            putString(k, str)
        },
    )

private class SharedPref<T>(
    private val context: Context,
    private val key: String,
    private val getter: SharedPreferences.(String) -> T?,
    private val setter: (SharedPreferences.Editor).(String, T) -> SharedPreferences.Editor,
): ReadWriteProperty<Any?, T?> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        if (!context.pref.contains(key)) return null

        return context.pref.getter(key)
    }

    @SuppressLint("CommitPrefEdits")
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        if (value == null) {
            context.pref.edit().remove(key).apply()
            return
        }

        context.pref.edit().setter(key, value).apply()
    }
}

class SharedPrefFactory<T>(
    private val context: Context,
    private val getter: SharedPreferences.(String) -> T?,
    private val setter: (SharedPreferences.Editor).(String, T) -> SharedPreferences.Editor,
): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, T?>> {
    override fun provideDelegate(
        thisRef: Any?,
        property: KProperty<*>
    ): ReadWriteProperty<Any?, T?> {
        return SharedPref(context, property.name, getter, setter)
    }
}
