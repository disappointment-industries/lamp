package site.mikesweb.lamp.api

import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Lamp(
    val name: String,
    val isOn: Boolean,
    val lastCheck: Instant,
)

@Serializable
sealed class Response

@Serializable
@SerialName("success")
data class Success(
    val data: List<Lamp>,
): Response()

@Serializable
@SerialName("error")
data class Error(
    val error: String,
): Response()