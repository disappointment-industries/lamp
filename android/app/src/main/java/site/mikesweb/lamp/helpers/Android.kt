package site.mikesweb.lamp.helpers

import android.content.Context
import android.widget.Toast
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.graphics.Color
import site.mikesweb.lamp.model.LampState

fun <T> SnapshotStateList<T>.updateFrom(list: List<T>) {
    // remove entries that aren't present in new list
    removeAll { !list.contains(it) }

    // update ones that are present
    list
        .associateWith { this.indexOf(it) }
        .forEach {
            if (it.value > 0) {
                if(this[it.value] != it.key) {
                    this[it.value] = it.key
                }
            }
        }

    // add new entries
    addAll(list.filter { !this.contains(it) })

    sortBy { list.indexOf(it) }
}

fun Context.toast(text: String, length: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, text, length).show()

val Color.cardColor @Composable get() = CardDefaults.cardColors(
    containerColor = this,
    contentColor = MaterialTheme.colorScheme.contentColorFor(this),
)