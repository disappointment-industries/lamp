package site.mikesweb.lamp.widget

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.compose.runtime.Composable
import androidx.glance.GlanceId
import androidx.glance.action.ActionParameters
import androidx.glance.action.actionParametersOf
import androidx.glance.appwidget.action.ActionCallback
import site.mikesweb.lamp.R
import site.mikesweb.lamp.WidgetWorker
import site.mikesweb.lamp.api.LämpClient
import site.mikesweb.lamp.helpers.longPref
import site.mikesweb.lamp.helpers.sharedPref
import site.mikesweb.lamp.helpers.stringPref
import site.mikesweb.lamp.helpers.toast
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.LampState
import site.mikesweb.lamp.model.SavedLampGroup
import site.mikesweb.lamp.widget.Widget.Companion.TYPE_LAMP
import site.mikesweb.lamp.widget.Widget.Companion.TYPE_LAMPGROUP

class ToggleLamp: ActionCallback {
    private var type = ""
    private var name = ""
    private var state = LampState.LostInSpace
    private var notfound = false

    override suspend fun onRun(context: Context, glanceId: GlanceId, parameters: ActionParameters) {
        type = parameters[KEY_TYPE]!!
        name = parameters[KEY_NAME]!!
        state = parameters[KEY_STATE]!!
        notfound = parameters[KEY_NOTFOUND]!!

        if (notfound) {
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                context.toast(context.getString(R.string.not_found))
            }
            return
        }

        val id by stringPref(context)
        val expires by longPref(context)
        val lampGroups by sharedPref<List<SavedLampGroup>>(context)
        val client = LämpClient(id, expires)
        try {
            when (type) {
                TYPE_LAMP -> {
                    client.toggleLamp(name)
                    context.getAllLampsAndRefresh()
                }
                TYPE_LAMPGROUP -> {
                    lampGroups!!.first { it.name == name }.lampNames.forEach {
                        client.toggleLamp(it)
                    }
                    context.getAllLampsAndRefresh()
                }
            }
        } catch (e: Exception) {
            val handler = Handler(Looper.getMainLooper())
            e.message?.let {
                handler.post {
                    context.toast(it)
                }
            }
            e.printStackTrace()
        }
    }

    private suspend fun LämpClient.toggleLamp(name: String) {
        when (state) {
            LampState.Off -> lampOn(name)
            LampState.On -> lampOff(name)
            LampState.LostInSpace -> {}
        }
    }

    companion object {
        val KEY_TYPE = ActionParameters.Key<String>("type")
        val KEY_NAME = ActionParameters.Key<String>("name")
        val KEY_STATE = ActionParameters.Key<LampState>("state")
        val KEY_NOTFOUND = ActionParameters.Key<Boolean>("notfound")
    }
}