package site.mikesweb.lamp.composables

import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import site.mikesweb.lamp.helpers.*
import site.mikesweb.lamp.model.Toggleable


private val selectedColor @Composable get() = MaterialTheme.colorScheme.tertiaryContainer

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun LämpCard(
    lamp: Toggleable,
    selected: Boolean = false,
    longClick: () -> Unit = {},
    click: () -> Unit = {},
) {

    val color by animateColorAsState(
        targetValue = if (selected) selectedColor else lamp.state.color
    )
    val contentColor by animateColorAsState(
        targetValue = MaterialTheme.colorScheme.contentColorFor(if (selected) selectedColor else lamp.state.color)
    )

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .combinedClickable(
                onClick = click,
                onLongClick = longClick,
            ),
        colors = CardDefaults.cardColors(
            containerColor = color,
            contentColor = contentColor,
        ),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp),
        ) {
            Crossfade(
                lamp.state.icon,
                modifier = Modifier.fillMaxWidth(),
            ) { icon ->
                Icon(
                    modifier = Modifier
                        .height(32.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally),
                    painter = painterResource(icon),
                    contentDescription = lamp.state.stateDescription,
                )
            }
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                text = lamp.name,
            )
        }
    }
}