package site.mikesweb.lamp.widget.config

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import site.mikesweb.lamp.composables.*

@Composable
fun LampList() {
    val lamps = LocalLamps.current
    val groups = LocalLampGroups.current
    val loaded = LocalLampsLoaded.current
    val done = done


    Crossfade(loaded) { load ->
        if (load) {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                contentPadding = PaddingValues(16.dp),
            ) {
                items(
                    items = groups,
                    key = { it.name },
                ) {
                    LämpCard(
                        lamp = it,
                        click = {
                            done(it)
                        },
                    )
                }

                items(
                    items = lamps,
                    key = { it.name },
                ) {
                    LämpCard(
                        lamp = it,
                        click = {
                            done(it)
                        },
                    )
                }
            }
        } else {
            Loading()
        }
    }
}