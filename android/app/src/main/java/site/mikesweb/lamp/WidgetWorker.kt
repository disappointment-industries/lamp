package site.mikesweb.lamp

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import site.mikesweb.lamp.api.Error
import site.mikesweb.lamp.api.LämpClient
import site.mikesweb.lamp.api.Success
import site.mikesweb.lamp.helpers.longPref
import site.mikesweb.lamp.helpers.sharedPref
import site.mikesweb.lamp.helpers.stringPref
import site.mikesweb.lamp.helpers.toLamp
import site.mikesweb.lamp.model.SavedLampGroup
import site.mikesweb.lamp.model.toLampGroup
import site.mikesweb.lamp.widget.getAllLampsAndRefresh
import site.mikesweb.lamp.widget.refreshWidgets

class WidgetWorker(
    private val context: Context,
    workerParameters: WorkerParameters
): CoroutineWorker(context, workerParameters) {
    override suspend fun doWork(): Result {
        try {
            context.getAllLampsAndRefresh()
        } catch (e: Exception) {
            return Result.retry()
        }

        return Result.success()
    }
}