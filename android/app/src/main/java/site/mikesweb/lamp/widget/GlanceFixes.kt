package site.mikesweb.lamp.widget

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.view.ViewConfiguration
import androidx.annotation.DrawableRes
import androidx.compose.material3.Card
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.*
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.glance.unit.Dimension
import site.mikesweb.lamp.helpers.color
import site.mikesweb.lamp.helpers.icon
import androidx.glance.LocalContext as GlanceLocalContext

@Composable
fun GlanceFixes(
    content: @Composable () -> Unit,
) {
    val context = GlanceLocalContext.current
    val config = context.resources.configuration
    val density = Density(context)
    val direction = if (config.layoutDirection == 1) LayoutDirection.Rtl else LayoutDirection.Ltr
    val viewConfig = AndroidViewConfiguration(ViewConfiguration.get(context))
    CompositionLocalProvider(
        LocalContext provides context,
        LocalConfiguration provides config,
        LocalDensity provides density,
        LocalLayoutDirection provides direction,
        LocalViewConfiguration provides viewConfig,
        LocalContentColor provides MaterialTheme.colorScheme.primary,
        content = content,
    )
}

fun Color.toColorFilter() = PorterDuffColorFilter(
    toArgb(),
    PorterDuff.Mode.SRC_ATOP
)

@Composable
fun getColorDrawable(
    @DrawableRes id: Int,
    color: Color = LocalContentColor.current,
): Drawable {
    val context = LocalContext.current
    val drawable = context.getDrawable(id)!!
    drawable.colorFilter = color.toColorFilter()
    return drawable
}

@Composable
fun Dp.toPx(): Int {
    val dpi = LocalConfiguration.current.densityDpi
    return (value * (dpi.toFloat() / 160)).toInt()
}

val Drawable.aspectratio: Float get() = intrinsicHeight.toFloat() / intrinsicWidth.toFloat()