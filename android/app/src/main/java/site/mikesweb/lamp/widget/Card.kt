package site.mikesweb.lamp.widget

import android.os.Build
import android.widget.RemoteViews
import androidx.compose.foundation.background
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.unit.dp
import androidx.core.graphics.drawable.toBitmap
import androidx.glance.GlanceModifier
import androidx.glance.ImageProvider
import androidx.glance.LocalSize
import androidx.glance.appwidget.appWidgetBackground
import androidx.glance.appwidget.cornerRadius
import androidx.glance.background
import androidx.glance.layout.Alignment
import androidx.glance.layout.Column
import androidx.glance.layout.padding
import site.mikesweb.lamp.R

@Composable
fun Card(
    modifier: GlanceModifier = GlanceModifier,
    background: Color = MaterialTheme.colorScheme.primaryContainer,
    content: @Composable () -> Unit,
) {
    val size = LocalSize.current
    val mod = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        modifier
            .background(background)
            .cornerRadius(16.dp)
    } else {
        val bg = getColorDrawable(R.drawable.widget_card_bg, background)
        modifier
            .background(ImageProvider(bg.toBitmap(size.width.toPx(), size.height.toPx())))
    }
    Column(
        modifier = mod
            .padding(8.dp)
            .appWidgetBackground(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        CompositionLocalProvider(
            LocalContentColor provides MaterialTheme.colorScheme.contentColorFor(background)
        ) {
            content()
        }
    }
}