package site.mikesweb.lamp.widget

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.graphics.drawable.toBitmap
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.glance.*
import androidx.glance.action.actionParametersOf
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.GlanceAppWidgetReceiver
import androidx.glance.appwidget.SizeMode
import androidx.glance.appwidget.action.actionRunCallback
import androidx.glance.layout.Spacer
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.height
import androidx.glance.layout.size
import androidx.glance.state.PreferencesGlanceStateDefinition
import site.mikesweb.lamp.helpers.color
import site.mikesweb.lamp.helpers.icon
import site.mikesweb.lamp.helpers.stateDescription
import site.mikesweb.lamp.model.LampState
import site.mikesweb.lamp.ui.theme.LämpMaterial
import site.mikesweb.lamp.R

class Widget: GlanceAppWidget() {
    override val stateDefinition = PreferencesGlanceStateDefinition
    override val sizeMode = SizeMode.Exact

    companion object {
        val KEY_TYPE = stringPreferencesKey("type")
        val KEY_NAME = stringPreferencesKey("name")
        val KEY_NOTFOUND = booleanPreferencesKey("notfound")
        val KEY_STATE = intPreferencesKey("state")

        val TYPE_LAMP = "lamp"
        val TYPE_LAMPGROUP = "lampGroup"
    }

    @Composable
    override fun Content() {
        GlanceFixes {
            LämpMaterial {
                val type = currentState(KEY_TYPE) ?: ""
                val notFound = currentState(KEY_NOTFOUND) ?: false
                val name = if (notFound) {
                    stringResource(R.string.not_found)
                } else {
                    currentState(KEY_NAME) ?: ""
                }

                val prefState = currentState(KEY_STATE)
                val state = LampState.values().firstOrNull {
                    it.ordinal == prefState
                } ?: LampState.LostInSpace
                Card(
                    modifier = GlanceModifier
                        .fillMaxSize()
                        .clickable(actionRunCallback<ToggleLamp>(actionParametersOf(
                            ToggleLamp.KEY_NAME to name,
                            ToggleLamp.KEY_TYPE to type,
                            ToggleLamp.KEY_STATE to state,
                            ToggleLamp.KEY_NOTFOUND to notFound,
                        ))),
                    background = state.color,
                ) {
                    val drawable = getColorDrawable(state.icon)

                    val size = 24.dp
                    val width = (drawable.aspectratio * size.value).dp
                    println("$width $size (${drawable.aspectratio})")
                    Image(
                        modifier = GlanceModifier,
                        provider = ImageProvider(drawable.toBitmap(size.toPx(), width.toPx())),
                        contentDescription = state.stateDescription,
                    )
                    Spacer(
                        modifier = GlanceModifier.height(8.dp)
                    )
                    Text(name)
                }
            }
        }
    }
}

class WidgetReceiver: GlanceAppWidgetReceiver() {
    override val glanceAppWidget: GlanceAppWidget = Widget()
}