package site.mikesweb.lamp.composables

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import site.mikesweb.lamp.R
import site.mikesweb.lamp.api.LämpClient

@Composable
fun LämpBar(
    text: String = stringResource(R.string.app_name)
) {
    SmallTopAppBar(
        title = { Text(text) },
        actions = {
            val user = LocalUser.current
            if (user == null) {
                val context = LocalContext.current

                TextButton(onClick = {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse("${LämpClient.baseUrl}/app_auth")
                    context.startActivity(intent)
                    (context as? Activity)?.finish()
                }) {
                    Text(stringResource(R.string.login))
                }
            } else {
                Text(
                    modifier = Modifier.padding(horizontal = 8.dp),
                    text = user,
                    style = MaterialTheme.typography.labelLarge,
                )
            }
        },
    )
}