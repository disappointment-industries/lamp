package site.mikesweb.lamp.composables

import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.platform.LocalContext
import site.mikesweb.lamp.helpers.sharedPref
import site.mikesweb.lamp.model.SavedLampGroup

val LocalGroups = compositionLocalOf<SnapshotStateList<SavedLampGroup>> { throw Error("No list provided") }

@Composable
fun GroupProvider(content: @Composable ()->Unit) {
    val context = LocalContext.current
    var lampGroups by sharedPref<List<SavedLampGroup>>(context)
    val groups = remember { mutableStateListOf<SavedLampGroup>() }

    LaunchedEffect(Unit) {
        lampGroups?.forEach { groups += it }
    }

    LaunchedEffect(Unit) {
        snapshotFlow { groups.toList() }
            .collect {
                lampGroups = it
            }
    }

    CompositionLocalProvider(
        LocalGroups provides groups,
    ) {
        content()
    }
}