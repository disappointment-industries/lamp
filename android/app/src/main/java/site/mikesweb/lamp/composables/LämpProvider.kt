package site.mikesweb.lamp.composables

import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.platform.LocalContext
import androidx.glance.appwidget.updateAll
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.delay
import site.mikesweb.lamp.api.Error
import site.mikesweb.lamp.api.Success
import site.mikesweb.lamp.helpers.toLamp
import site.mikesweb.lamp.helpers.toast
import site.mikesweb.lamp.helpers.updateFrom
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.LampGroup
import site.mikesweb.lamp.model.toLampGroup
import site.mikesweb.lamp.widget.Widget

val LocalLamps = compositionLocalOf<SnapshotStateList<Lamp>> { error("No LocalLamps provided") }
val LocalLampsLoaded = compositionLocalOf<Boolean> { error("No LocalLampsLoaded provided") }
val LocalLampGroups = compositionLocalOf<SnapshotStateList<LampGroup>> { error("No LocalLampGroups provided") }

@Composable
fun LämpProvider(
    content: @Composable ()->Unit,
) {
    ClientProvider {
        GroupProvider {


            val client = LocalClient.current
            val lamps = remember { mutableStateListOf<Lamp>() }
            val savedGroups = LocalGroups.current
            val groups = remember { mutableStateListOf<LampGroup>() }
            val context = LocalContext.current
            var loaded by remember { mutableStateOf(false) }

            LaunchedEffect(Unit) {
                while (true) {
                    try {
                        val response = client.getLamps()
                        when (response) {
                            is Success -> {
                                lamps.updateFrom(response.data.map { it.toLamp() })
                                loaded = true
                            }
                            is Error -> context.toast(response.error)
                        }
                    } catch (c: CancellationException) {
                        throw c
                    } catch (e: Exception) {
                        e.message?.let { context.toast(it) }
                        e.printStackTrace()
                    }
                    delay(1000)
                }
            }

            // redraw all groups if there were changes in the groups itself
            LaunchedEffect(savedGroups.toList()) {
                groups.clear()
                groups.addAll(savedGroups.map { it.toLampGroup(lamps) })
            }

            // redraw a given group if it is needed
            LaunchedEffect(lamps.toList()) {
                val toDo = mutableListOf<()->Unit>()
                groups.forEachIndexed { i, it ->
                    val group = savedGroups[i].toLampGroup(lamps)
                    if (
                    // the group's state has changed or
                        it.state != group.state ||
                        // the lamps that are in the group and API changed
                        it.lamps.map { it.name } != group.lamps.map { it.name }
                    ) {
                        toDo += { groups[i] = group }
                    }
                }
                toDo.forEach { it() }
            }

            CompositionLocalProvider(
                LocalLamps provides lamps,
                LocalLampGroups provides groups,
                LocalLampsLoaded provides loaded,
            ){
                WidgetRefresher()
                content()
            }
        }
    }
}