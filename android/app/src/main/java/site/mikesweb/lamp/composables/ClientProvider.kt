package site.mikesweb.lamp.composables

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import site.mikesweb.lamp.api.LämpClient
import site.mikesweb.lamp.helpers.longPref
import site.mikesweb.lamp.helpers.stringPref

val LocalClient = compositionLocalOf { LämpClient() }
val LocalUser = compositionLocalOf<String?> { null }

@Composable
fun ClientProvider(
    content: @Composable () -> Unit,
) {
    val context = LocalContext.current

    val id by stringPref(context)
    val expires by longPref(context)
    val name by stringPref(context)

    val client = remember { LämpClient(id, expires) }

    val username = remember { name }

    CompositionLocalProvider(
        LocalClient provides client,
        LocalUser provides username
    ) {
        content()
    }
}