package site.mikesweb.lamp.composables

import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import site.mikesweb.lamp.R

@Composable
fun LampGroupDropdown(
    expanded: Boolean,
    name: String,
    dismiss: () -> Unit,
) {
    val savedGroups = LocalGroups.current
    val isFirst = savedGroups.firstOrNull()?.name == name
    val isLast = savedGroups.lastOrNull()?.name == name
    val idx = savedGroups.indexOfFirst { it.name == name }

    DropdownMenu(
        expanded = expanded,
        onDismissRequest = dismiss,
        offset = DpOffset(8.dp, 0.dp)
    ) {
        if (!isFirst) {
            DropdownMenuItem(
                leadingIcon = {
                    Icon(
                        painter = painterResource(R.drawable.up),
                        contentDescription = stringResource(R.string.move_up)
                    )
                },
                text = { Text(stringResource(R.string.move_up)) },
                onClick = {
                    val prev = savedGroups[idx-1]
                    savedGroups[idx-1] = savedGroups[idx]
                    savedGroups[idx] = prev
                },
            )
        }
        if (!isLast) {
            DropdownMenuItem(
                leadingIcon = {
                    Icon(
                        painter = painterResource(R.drawable.down),
                        contentDescription = stringResource(R.string.move_down)
                    )
                },
                text = { Text(stringResource(R.string.move_down)) },
                onClick = {
                    val next = savedGroups[idx+1]
                    savedGroups[idx+1] = savedGroups[idx]
                    savedGroups[idx] = next
                },
            )
        }
        DropdownMenuItem(
            leadingIcon = {
                Icon(
                    painter = painterResource(R.drawable.trash),
                    contentDescription = stringResource(R.string.delete)
                )
            },
            text = { Text(stringResource(R.string.delete)) },
            onClick = {
                dismiss()
                savedGroups.removeAll { g -> g.name == name }
            },
        )
    }
}