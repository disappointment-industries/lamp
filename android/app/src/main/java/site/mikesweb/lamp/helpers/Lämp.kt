package site.mikesweb.lamp.helpers

import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import kotlinx.datetime.Clock
import site.mikesweb.lamp.R
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.api.LämpClient
import site.mikesweb.lamp.api.Lamp as ApiLamp
import site.mikesweb.lamp.model.LampState
import site.mikesweb.lamp.model.Toggleable
import java.lang.Exception
import kotlin.time.ExperimentalTime
import kotlin.time.Duration.Companion.minutes

val LampState.color: Color @Composable get() = when (this) {
    LampState.On -> MaterialTheme.colorScheme.primaryContainer
    LampState.Off -> MaterialTheme.colorScheme.secondaryContainer
    LampState.LostInSpace -> MaterialTheme.colorScheme.errorContainer
}

val LampState.icon: Int get() = when (this) {
    LampState.On -> R.drawable.light_on
    LampState.Off -> R.drawable.light_off
    LampState.LostInSpace -> R.drawable.offline
}

val LampState.stateDescription: String @Composable get() = stringResource(when (this) {
    LampState.On -> R.string.lamp_on
    LampState.Off -> R.string.lamp_off
    LampState.LostInSpace -> R.string.lamp_offline
})

fun ApiLamp.toLamp() = Lamp(
    name = name,
    state = when {
        lastCheck < Clock.System.now() - 5.minutes -> LampState.LostInSpace
        isOn -> LampState.On
        else -> LampState.Off
    }
)