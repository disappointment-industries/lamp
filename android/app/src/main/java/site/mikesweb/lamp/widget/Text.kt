package site.mikesweb.lamp.widget

import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.glance.GlanceModifier
import androidx.glance.text.TextStyle
import androidx.glance.unit.ColorProvider

@Composable
fun Text(
    text: String,
    modifier: GlanceModifier = GlanceModifier,
    style: TextStyle? = TextStyle(color = ColorProvider(LocalContentColor.current)),
    maxLines: Int = Int.MAX_VALUE,
) {
    androidx.glance.text.Text(
        text = text,
        style = style,
        modifier = modifier,
        maxLines = maxLines,
    )
}