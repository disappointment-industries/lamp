package site.mikesweb.lamp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import site.mikesweb.lamp.composables.*
import site.mikesweb.lamp.helpers.longPref
import site.mikesweb.lamp.helpers.stringPref
import site.mikesweb.lamp.ui.theme.LämpTheme

class MainActivity : ComponentActivity() {
    var id by stringPref(this)
    var expires by longPref(this)
    var name by stringPref(this)

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleIntent()

        setContent {
            LämpProvider {
                LämpTheme {
                    Scaffold(
                        modifier = Modifier.fillMaxSize(),
                        topBar = { LämpBar() },
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(it),
                        ) {
                            MainView()
                        }
                    }
                }
            }
        }
    }

    private fun handleIntent() {
        if (
            intent.data != null
            && intent.data?.scheme == "lamp"
            && intent.data?.host == "login"
        ) {
            val id = intent.data?.getQueryParameter("id") ?: return
            val name = intent.data?.getQueryParameter("name") ?: return
            val expires = intent.data?.getQueryParameter("expires")?.toLongOrNull() ?: return

            this.id = id
            this.name = name
            this.expires = expires
        }
    }
}
