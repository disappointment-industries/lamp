package site.mikesweb.lamp.ui.theme

import androidx.compose.ui.graphics.Color

val DarkPrimary = Color(0xFFffba2f)
val DarkSecondary = Color(0xFFa5c8ff)
val DarkTertiary = Color(0xFFEFB8C8)

val LightPrimary = Color(0xFF7f5600)
val LightSecondary = Color(0xFF005eb5)
val LightTertiary = Color(0xFF7D5260)