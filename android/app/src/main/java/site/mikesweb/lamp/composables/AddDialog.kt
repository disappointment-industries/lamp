package site.mikesweb.lamp.composables

import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import site.mikesweb.lamp.R
import site.mikesweb.lamp.helpers.toast
import site.mikesweb.lamp.model.Lamp
import site.mikesweb.lamp.model.SavedLampGroup

@Composable
fun AddDialog(
    visible: Boolean,
    dismiss: () -> Unit,
    selected: SnapshotStateList<Lamp>,
) {
    val savedGroups = LocalGroups.current
    var name by remember { mutableStateOf("") }
    val context = LocalContext.current

    if (visible) {
        AlertDialog(
            onDismissRequest = dismiss,
            dismissButton = {
                TextButton(onClick = dismiss) {
                    Text(stringResource(R.string.cancel))
                }
            },
            confirmButton = {
                val sameError = stringResource(R.string.already_exists)
                Button(onClick = {
                    if (savedGroups.any { it.name == name }) {
                        context.toast(sameError)
                        return@Button
                    }
                    savedGroups.add(
                        SavedLampGroup(
                            name = name,
                            lampNames = selected.map { it.name },
                        )
                    )
                    selected.clear()
                    name = ""
                    dismiss()
                }) {
                    Text(stringResource(R.string.add_group))
                }
            },
            title = {
                Text(stringResource(R.string.choose_name))
            },
            text = {
                OutlinedTextField(
                    isError = savedGroups.any { it.name == name },
                    value = name,
                    onValueChange = { name = it },
                    label = { Text(stringResource(R.string.name)) }
                )
            }
        )
    }
}